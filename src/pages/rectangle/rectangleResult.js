import React, {useEffect, useState} from 'react';

const RectangleResult = ({rectRef}) => {    
  const [field, setField] = useState();
  
  useEffect(()=>{    
    let width = rectRef.current.attributes[0].value;      
    let height = rectRef.current.attributes[1].value;
    setField(Math.floor(width * height));     
    })
    
  
  return ( 
  <>
  <p style={{color:"white"}}>objętość to: {field}px</p>
  </>
  )
  }
 
export default RectangleResult;
