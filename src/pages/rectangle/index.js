import React, { useState, useRef } from 'react';
import styled from 'styled-components';
import RectangleResult from './rectangleResult';

const MyRectangle = styled.div`
width:${props => props.width+'px'};
height:${props => props.height+'px'};
background-color: black;
display: flex;
text-align: center;
align-items: center;
justify-content: center;
transition: all 0.5s;
`

const Rectangle = () => {

  const rectRef = useRef();
  const [width, setWidth] = useState(103);
  const [height, setHeight] = useState(120);
 
   
  const handleResize = () => {    
    const max = 500;
    const min = 100;    
    let height = (Math.floor(Math.random() * (max - min) ) + min);
    let width = (Math.floor(Math.random() * (max - min) ) + min);    
    setWidth(width);
    setHeight(height);    
   }
 return ( 
  <>
  <div className="rectangle__wrapper"> 
  <MyRectangle    
    ref={rectRef}
    width={width}
    height={height}
  >   
    <RectangleResult {...{rectRef}}/>
    
  </MyRectangle>
  <p>Szerokość: {width}px</p>
  <p>Wysokość: {height}px</p>
  <button onClick={()=>handleResize()}>Generuj nowy element</button>
  </div>
  </>
  );
}
 
export default Rectangle;